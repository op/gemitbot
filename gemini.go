package main

import (
	"bufio"
	"context"
	"errors"
	"regexp"
	"strings"

	gemini "git.sr.ht/~adnano/go-gemini"
)

var (
	ErrNoTitle = errors.New(`no title found`)

	title1 = regexp.MustCompile(`^#[^#]`)
	title2 = regexp.MustCompile(`^##[^#]`)
	title3 = regexp.MustCompile(`^###`)
)

func geminiTitle(url string) (string, error) {
	client := gemini.Client{}
	ctx := context.Background()
	resp, err := client.Get(ctx, url)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	if resp.Status != 20 || !strings.HasPrefix(resp.Meta, "text/gemini") {
		return "", ErrNoTitle
	}

	var t2, t3 string

	scanner := bufio.NewScanner(resp.Body)
	for scanner.Scan() {
		line := scanner.Text()

		switch {
		case title1.MatchString(line):
			return line, nil
		case title2.MatchString(line) && t2 == "":
			t2 = line
		case title3.MatchString(line) && t3 == "":
			t3 = line
		}
	}

	if t2 != "" {
		return t2, nil
	}

	if t3 != "" {
		return t3, nil
	}

	return "", ErrNoTitle
}
