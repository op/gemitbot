module gemitbot

go 1.16

require (
	git.sr.ht/~adnano/go-gemini v0.2.2
	github.com/andybalholm/cascadia v1.3.1
	github.com/fluffle/goirc v1.1.1
	golang.org/x/net v0.0.0-20210916014120-12bc252f5db8
)
